from quora_app import app
from flask import render_template, g, flash, redirect, url_for, request, abort, current_app, jsonify
from flask import send_file
from quora_app.forms import LoginForm, AddQuestionForm, WriteAnswerForm
from flask_login import current_user, login_user
from quora_app.models import User, Answer, Question
from flask_login import logout_user, login_required
from quora_app import db
from quora_app.forms import RegistrationForm, SearchForm
from flask_babel import _, get_locale
import os
import time
from celery import Celery
from quora_app.tasks import download_data_task


@app.before_request
def before_request():
    if current_user.is_authenticated:
        g.search_form = SearchForm()
    g.locale = str(get_locale())


@app.route('/', methods=['GET', 'POST'])
@app.route('/home', methods=['GET', 'POST'])
def home():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('home'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))
    return render_template('home.html', title='Sign In', form=form)


@app.route('/questions')
def index():
    questions = Question.query.all()
    if current_user.is_authenticated:
        user = User.query.filter_by(username=current_user.username)
        if len(questions) > 0:
            return render_template('index.html', title='home', user=user, questions=questions)
        else:
            return render_template('index.html', title='home', user=user)
    else:
        return render_template('index.html', title='home', questions=questions)


@app.route('/questions/<question_id>/answers/')
def answer(question_id):
    answers = Answer.query.filter_by(question_id=question_id)
    question = Question.query.get(question_id)
    if answers.count() > 0:
        return render_template('answers.html', answers=answers, questionid=question_id, question=question)
    else:
        return render_template('answers.html', questionid=question_id, question= question)


@app.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('index'))
    page = request.args.get('page', 1, type=int)
    questions, total = Question.search(g.search_form.q.data, page)
    user = User.query.filter_by(username=current_user.username)
    if total > 0:
        return render_template('index.html', title='home', user=user, questions=questions)
    else:
        return render_template('index.html', title='home', user=user)


@app.route('/user/<username>')
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    questions = Question.query.filter_by(user_id=current_user.id)
    answers = Answer.query.filter_by(user_id=current_user.id)
    return render_template('user.html', user=user, questions=questions, answers=answers)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route('/newquestions', methods=['GET', 'POST'])
def add_question():
    form = AddQuestionForm()
    if form.validate_on_submit():
        question = Question(body=form.question.data, user_id=current_user.id)
        db.session.add(question)
        db.session.commit()
        question = Question.query.filter_by(body=form.question.data)
        flash('Congratulations, you added a new question!')
        return redirect(url_for('answer', question_id=question.id))
    else:
        return render_template('addquestion.html', title='Question', form=form)


@app.route('/questions/<question_id>/answer/new', methods=['GET', 'POST'])
def addanswer(question_id):
    if current_user.is_authenticated:
        form = WriteAnswerForm()
        if form.validate_on_submit():
            answer = Answer(body=form.answer.data, user_id=current_user.id, question_id=question_id)
            db.session.add(answer)
            db.session.commit()
            flash('Congratulations, you added a new answer!')
            return redirect(url_for('answer', question_id=question_id))
        else:
            return render_template('addanswer.html', title='Answer', form=form)
    else:
        flash('Please Register to write answer')
        return redirect(url_for('home'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        login_user(user)
        return redirect(url_for('home'))
    return render_template('register.html', title='Register', form=form)


@app.route('/updatequestion/<questionid>', methods=['GET', 'POST'])
def updatequestion(questionid):
    question = Question.query.get_or_404(questionid)
    if current_user.is_authenticated:
        form = AddQuestionForm()
        if form.validate_on_submit():
            question.body = form.question.data
            db.session.commit()
            flash('question updated')
            return redirect(url_for('user', username=current_user.username))
        elif request.method == 'GET':
            form.question.data = question.body
        return render_template('addquestion.html', title='Question', form=form)


@app.route('/updateanswer/<answerid>', methods=['GET', 'POST'])
def updateanswer(answerid):
    answer = Answer.query.get_or_404(answerid)
    question = Question.query.filter_by(id=answer.question_id)
    if current_user.is_authenticated:
        form = WriteAnswerForm()
        if form.validate_on_submit():
            answer.body = form.answer.data
            db.session.commit()
            flash('question updated')
            return redirect(url_for('user', username=current_user.username))
        elif request.method == 'GET':
            form.answer.data = answer.body
        return redirect('addanswer.html', title='Answer', form=form)


@app.route('/deletequestion/<questionid>')
def deletequestion(questionid):
    if current_user.is_authenticated:
        Answer.query.filter_by(question_id=questionid).delete()
        Question.query.filter_by(id=questionid).delete()
        db.session.commit()
        return redirect(url_for('user', username=current_user.username))


@app.route('/deleteanswer/<answerid>')
def deleteanswer(answerid):
    if current_user.is_authenticated:
        Answer.query.filter_by(id=answerid).delete()
        db.session.commit()
        return redirect(url_for('user', username=current_user.username))


@app.route('/deleteuser/<userid>')
def deleteuser(userid):
    if current_user.is_authenticated:
        Answer.query.filter_by(user_id=userid).delete()
        Question.query.filter_by(user_id=userid).delete()
        User.query.filter_by(id=userid).delete()
        db.session.commit()
        return redirect(url_for('logout'))


@app.route("/download", methods=['POST'])
def download():
    download = download_data_task.delay(current_user.id)
    return jsonify({}), 202, {'Location': url_for('download_status', download_id=download.id)}


@app.route('/download/<path:filepath>', methods=['GET'])
def download_data(filepath):
    return send_file(filepath)


@app.route('/downladstatus/<download_id>')
def download_status(download_id):
    download = download_data_task.AsyncResult(download_id)
    if not download.ready():
        return jsonify({
            'status': 'PENDING'
        })
    else:
        result = download.get()
        filepath = result['filepath']
        # redirect to '/download/<filepath>`
    if download.state == 'PENDING':
        response = {
            'state': download.state,
            'status': 'Pending...'
        }
    elif download.state != 'FAILURE':
        response = {
            'state': download.state,
            'status': download.info.get('status', '')
        }
        if 'filepath' in download.info:
            response['filepath'] = download.info['filepath']
    else:
        response = {
            'state': download.state,
            'status': str(download.info)
        }
    return jsonify(response)
