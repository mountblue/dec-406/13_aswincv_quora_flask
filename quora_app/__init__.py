from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from elasticsearch import Elasticsearch
from flask_babel import Babel
import os

# app = Flask(__name__)
# app.config.from_object(Config)
app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'
app.config['ELASTICSEARCH_URL'] = 'http://localhost:9200'
ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
babel = Babel(app)
app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']])

from quora_app import routes, models
