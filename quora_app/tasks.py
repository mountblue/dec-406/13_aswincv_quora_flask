from celery import Celery
from quora_app.models import Answer, Question
import csv

celery_app = Celery('quora_app.tasks', backend='rpc://', broker='pyamqp://')


@celery_app.task
def download_data_task(user_id):
    # business logic here. Remember to set appropriate path below.
    answers = Answer.query.filter(Answer.user_id == user_id)
    questions = Question.query.filter(Question.user_id == user_id)
    data_list = []
    for question in questions:
        data_list.append(question.body)
    for answer in answers:
        data_list.append(answer.body)
    filepath = 'quora_app/user_data/'+str(user_id)+'.csv'
    with open(filepath, 'w') as data_file:
        fileWriter = csv.writer(data_file, delimiter=",")
        fileWriter.writerow(['Question', 'Answer'])
        for row in data_list:
            fileWriter.writerow(row)

    return {
        'status': 'Download completed',
        'filepath': 'user_data/{}.csv'.format(user_id)
    }
