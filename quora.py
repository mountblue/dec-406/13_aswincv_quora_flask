from quora_app import app
from quora_app.models import User, Question, Answer
from celery import Celery


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Question': Question, 'Answer': Answer}
